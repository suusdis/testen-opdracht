using System;
using System.Reflection.Metadata;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Moq;

namespace AutoVerzekeringsPremieTest
{
    public class ProgramTest
    {
        [Fact]
        public void CheckIfWAPlusIsTwentyProcentExpensiver()
        {
            Vehicle vehicle = new Vehicle(1000, 4000, 2000);
            PolicyHolder policy = new PolicyHolder(40, "02-02-1999", 1187, 20);

            var WAPlus = InsuranceCoverage.WA_PLUS;
            var WA = InsuranceCoverage.WA;

            PremiumCalculation premium = new PremiumCalculation(vehicle, policy, WA);
            PremiumCalculation premiumPlus = new PremiumCalculation(vehicle, policy, WAPlus);

            double payment = premium.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);
            double paymentPlus = premiumPlus.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            Assert.Equal(26.17, payment);
            Assert.Equal(31.41, paymentPlus);
        }

        [Fact]
        public void LicenceAge()
        {
            var mockVehicle = new Mock<IVehicle>();
            mockVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(5000);

            PolicyHolder policy = new PolicyHolder(40, "02-02-1999", 1187, 20);

            PremiumCalculation premium = new PremiumCalculation(mockVehicle.Object, policy, InsuranceCoverage.WA);

            double payment = premium.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            Assert.Equal(5.74, payment);
        }

        /* mock example
         * [Fact]
        public void DriverUnderTwentyThreeGetsAHigherFine()
        {
            //Arrange

            var mockDriver = new Mock<IDriver>();
            mockDriver.Setup(driver => driver.Vehicle).Returns(VehicleType.Other);
            mockDriver.SetupSequence(driver => driver.DriverAge())
                .Returns(22)
                .Returns(23);
            
            var mockViolation = new Mock<IViolation>();
            mockViolation.Setup(violation => violation.Promillage).Returns(8);

            //Act
            IPenalty penalty = new Penalty(mockDriver.Object, mockViolation.Object);

            double firstFine = penalty.FineAmount();
            double secondFine = penalty.FineAmount();

            //Assert
            Assert.Equal(firstFine, secondFine + 100);
        }
        theory example
         [Theory]
        [InlineData(2.4)]
        public void HigherThenTwoButLowerThenFiveDotThreeDriverOlderThenTwentyThreePassengerCar(double promillage)
        {
            //Arrange

            IDriver driver = new Driver("01-01-1970", "01-01-1970", false, VehicleType.PassengerCar);
            IViolation violation = new Violation(promillage);

            //Act
            IPenalty penalty = new Penalty(driver, violation);
            double actualFine = penalty.FineAmount();

            //Assert
            Assert.Equal(242, actualFine);
        }
         */
    }
}
